#ifndef USER_H
#define USER_H
#include <QMetaType>

struct User{
    int id;
    QString name;
    QString login;
};
Q_DECLARE_METATYPE(User)
#endif // USER_H
