#ifndef TEAMMENU_H
#define TEAMMENU_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class teamMenu;
}

class teamMenu : public QDialog
{
    Q_OBJECT

public:
    explicit teamMenu(QWidget *parent = 0);
    ~teamMenu();

private:
    Ui::teamMenu *ui;

signals:
    void createTeam(const QString & name, const QString & country, const int & year, const QString & stadium);
private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
};

#endif // TEAMMENU_H
