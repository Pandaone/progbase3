#ifndef STORAGE_H
#define STORAGE_H

#include <vector>
#include <optional>
#include <fstream>
#include <QVariant>
#include "players.h"
#include "teams.h"
#include "image.h"

class Storage
{
   string dir_name_;

 public:
   Storage() {}
   explicit Storage(const string & dir_name): dir_name_{dir_name} {}
   virtual ~Storage() {}

   void setName(const string & dir_name);
   string name() const;

   virtual bool isOpen() const = 0;
   virtual bool open() = 0;
   virtual void close() = 0;

   virtual vector<Player> getAllPlayers(void) = 0;
   virtual optional<Player> getPlayerById(int player_id) = 0;
   virtual bool updatePlayer(const Player & player) = 0;
   virtual bool removePlayer(int player_id) = 0;
   virtual int insertPlayer(Player & player, int user_id = QVariant::Int) = 0;

   virtual vector<Team> getAllTeams(void) = 0;
   virtual optional<Team> getTeamById(int player_id) = 0;
   virtual bool updateTeam(const Team & team) = 0;
   virtual bool removeTeam(int team_id) = 0;
   virtual int insertTeam(Team & team) = 0;

   virtual vector<Player> getAllUserPlayers(int user_id) = 0;
   virtual vector<Team> getAllPlayerTeams(int player_id) = 0;
   virtual bool insertPlayerTeam(int player_id, int team_id) = 0;
   virtual bool removePlayerTeam(int player_id, int team_id) = 0;

   virtual QVariant authUser(QString const & username, QString const & password) = 0;
   virtual int registerUser(QString const & username, QString const & password, QString const & fullname) = 0;

   virtual vector<Player> getModifiedListOfPlayers(int from, const QString &str, const int & user_id, int amount = 10) = 0;
   virtual int getTotalAmoutOfEntities(const QString &str, const int & user_id) = 0;

   virtual bool insertPlayerImage(const Image & image, int player_id) = 0;
   virtual bool removePlayerImage(int player_id) = 0;
   virtual QVariant getPlayerImage(int player_id) = 0;
};


#endif // STORAGE_H
