#include "create_dialog.h"
#include "ui_create_dialog.h"

create_dialog::create_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::create_dialog)
{
    ui->setupUi(this);
}

create_dialog::~create_dialog(){
    delete ui;
}

void create_dialog::on_buttonBox_accepted(){
    if(ui->name->text().length() == 0 || ui->login->text().length() == 0 || ui->password->text().length() == 0 || ui->password2->text().length() == 0)
        QMessageBox::information(this, "Warning", "You should field all gaps!!!");
    else if(ui->password->text() != ui->password2->text()) QMessageBox::information(this, "Warning", "Passwords are not the same!!!");
    else emit registerUser(ui->login->text(), ui->password->text(), ui->name->text());
}

void create_dialog::on_buttonBox_rejected(){
    this->reject();
}

QString create_dialog::getPassword(){
    return ui->password->text();
}

QString create_dialog::getLogin(){
    return ui->login->text();
}

void create_dialog::on_checkBox_clicked(bool checked){
    if(checked){
        ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
        ui->password2->setEchoMode(QLineEdit::EchoMode::Normal);
    }
    else{
        ui->password->setEchoMode(QLineEdit::EchoMode::Password);
        ui->password2->setEchoMode(QLineEdit::EchoMode::Password);
    }
}

void create_dialog::user_acception(bool registreted){
    if(!registreted) QMessageBox::information(this, "Warning", "This login is already used");
    else{
        QMessageBox::information(this, "Welcome", "You have just created your account");
        this->accept();
    }
}
