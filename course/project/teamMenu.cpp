#include "teamMenu.h"
#include "ui_teamMenu.h"

teamMenu::teamMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::teamMenu)
{
    ui->setupUi(this);
}

teamMenu::~teamMenu(){
    delete ui;
}

void teamMenu::on_buttonBox_rejected(){
    this->reject();
}

void teamMenu::on_buttonBox_accepted(){
    if(ui->nameEdit->text().length() == 0 ||
            ui->countryEdit->text().length() == 0 ||
            ui->stadiumEdit->text().length() == 0){
        QMessageBox::information(this, "Warning", "You should field all gaps!!!");
        return;
    }
    emit createTeam(ui->nameEdit->text(), ui->countryEdit->text(), ui->spinBox->value(), ui->stadiumEdit->text());
    this->accept();
}
