#include "editdialog.h"
#include "ui_editdialog.h"
#include <QDebug>

EditDialog::EditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);

    ui->listWidget->setVisible(false);
    ui->comboBox->setVisible(false);
    ui->RmTeam->setVisible(false);
    ui->AddTeam->setVisible(false);
    ui->infoBtn->setVisible(false);
}

EditDialog::~EditDialog(){
    delete ui;
}

void EditDialog::setData(const Player & player){
    ui->name_->setText(QString::fromStdString(player.name));
    ui->age_->setValue(player.age);
    ui->position_->setText(QString::fromStdString(player.position));
    ui->team_->setText(QString::fromStdString(player.team));
    ui->country_->setText(QString::fromStdString(player.country));
    id = player.id;
}

Player EditDialog::getData(){
    Player player;
    player.name = ui->name_->text().toStdString();
    player.age = ui->age_->value();
    player.position = ui->position_->text().toStdString();
    player.team = ui->team_->text().toStdString();
    player.country = ui->country_->text().toStdString();
    return player;
}

void EditDialog::setPlaceHolderText(){
    ui->name_->setPlaceholderText("name...");
    ui->position_->setPlaceholderText("position...");
    ui->team_->setPlaceholderText("team...");
    ui->country_->setPlaceholderText("country...");
}

void EditDialog::setTeamsData(const vector<Team> & teams){
    ui->listWidget->setVisible(true);
    foreach(Team team, teams){
        QListWidgetItem * newItem = new QListWidgetItem(QString::fromStdString(team.name));
        QVariant var = QVariant::fromValue(team);
        newItem->setData(Qt::UserRole, var);
        ui->listWidget->addItem(newItem);
    }
}

void EditDialog::setAllTeamsData(const vector<Team> & teams){
    ui->comboBox->setVisible(true);
    ui->RmTeam->setVisible(true);
    ui->AddTeam->setVisible(true);
    ui->comboBox->addItem("Teams...");
    foreach(Team team, teams){
        QVariant var = QVariant::fromValue(team);
        ui->comboBox->addItem(QString::fromStdString(team.name), var);
    }
}

void EditDialog::on_RmTeam_clicked(){
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if(list.count() == 0) return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Delete","Are you sure?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes){
        foreach(QListWidgetItem * selectedItem, list){
            int row = ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem(row);
            QVariant var = selectedItem->data(Qt::UserRole);
            Team team = var.value<Team>();

            ui->comboBox->addItem(QString::fromStdString(team.name), var);
            emit PlayerTeam(id, team.id, false);

            delete selectedItem;
        }
    }
}

void EditDialog::on_AddTeam_clicked(){
    QVariant var = ui->comboBox->currentData();
    Team team = var.value<Team>();
    QListWidgetItem * newItem = new QListWidgetItem(QString::fromStdString(team.name));
    newItem->setData(Qt::UserRole, var);
    ui->listWidget->addItem(newItem);
    ui->comboBox->removeItem(ui->comboBox->currentIndex());
    ui->comboBox->setCurrentIndex(0);

    emit PlayerTeam(id, team.id, true);
}

void EditDialog::on_comboBox_currentIndexChanged(int index){
    if(index == 0) ui->AddTeam->setEnabled(false);
    else ui->AddTeam->setEnabled(true);
}

void EditDialog::on_buttonBox_accepted(){
    if(ui->name_->text().length() != 0 && ui->position_->text().length() != 0 && ui->team_->text().length() != 0 && ui->country_->text().length() != 0)
        accept();
    else QMessageBox::information(this, "Warning", "You should field all gaps!!!");
}

void EditDialog::on_buttonBox_rejected(){
    reject();
}

void EditDialog::on_infoBtn_clicked(){
    QListWidgetItem * var = ui->listWidget->selectedItems().at(0);
    Team team = var->data(Qt::UserRole).value<Team>();
    QMessageBox::information(this, QString::fromStdString(team.name),
                             "Country: " + QString::fromStdString(team.country) + "\n"
                             + "Found in " + QString::number(team.YearFound) + "\n"
                             + "Stadium: " + QString::fromStdString(team.stadium));
}

void EditDialog::on_listWidget_itemSelectionChanged(){
    int count = ui->listWidget->selectedItems().count();
    if(count == 0) ui->RmTeam->setEnabled(false);
    else if(count == 1){
        ui->RmTeam->setEnabled(true);
        ui->infoBtn->setVisible(true);
        return;
    }
    else ui->RmTeam->setEnabled(true);
    ui->infoBtn->setVisible(false);
}
