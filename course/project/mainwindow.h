#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "sqlstorage.h"
#include <QDebug>
#include <QVariant>
#include "editdialog.h"
#include "authorization_dialog.h"
#include "teamMenu.h"
#include "json.h"
#include "imageDialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void Logout();
    void OpenStorage();
    void NewStorage();
    void Exit();
    void AddTeam();
    void importData();
    void exportData();
    void userInfo();
    void ImageMenu();

    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_RmBtn_clicked();
    void on_AddBtn_clicked();
    void on_EditBtn_clicked();

    void PlayerTeam(int player_id, int team_id, bool code);

    void auth_user(const QString & login, const QString & password);
    void registration(QString const & username, QString const & password, QString const & fullname);

    void on_name_search_textChanged(const QString &arg1);

    void createTeam(const QString & name, const QString & country, const int & year, const QString & stadium);

    void on_goLeft_clicked();

    void on_minPage_clicked();

    void on_maxPage_clicked();

    void on_goRight_clicked();

    void on_listWidget_itemSelectionChanged();

    void addImageToDb(const Image & image, int player_id);


void removeImageFromDb(int player_id);
private:
    Ui::MainWindow *ui;

    Storage * storage_;

    User user;

    int total_amount = 0;

    void clearLabels();

    void updateLabels(const Player & player);

    void fillListWithPlayers(const vector<Player> & players);

    void addPlayer(const Player & player);

signals:
    void user_exist(bool exist);
    void user_registered(bool registered);
};

#endif // MAINWINDOW_H
