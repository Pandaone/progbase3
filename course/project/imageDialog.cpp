#include "imageDialog.h"
#include "ui_imageDialog.h"

imageDialog::imageDialog(const vector<Player> & players, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::imageDialog),
    players_(players)
{
    ui->setupUi(this);

    ui->comboBox->addItem(".png");

    foreach(Player player, players_) {
        QVariant var = QVariant::fromValue(player);
        ui->players->addItem(QString::fromStdString(player.name), var);
    }
}

imageDialog::~imageDialog(){
    delete ui;
}

void imageDialog::on_addBtn_clicked(){
    QString fileName = QFileDialog::getSaveFileName(this, "Dialog Caption", "./../../..", "PNG(*.png)");
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) return;
    QByteArray inByteArray = file.readAll();

    QPixmap inPixmap;
    inPixmap.loadFromData(inByteArray);
    ui->label->setPixmap(inPixmap);

    Image image;
    image.name = ui->lineEdit->text() + ui->comboBox->currentText();
    image.arr = inByteArray;

    QVariant var = ui->players->currentData();

    emit addImageToDb(image, var.value<Player>().id);
    QMessageBox::information(this, "Image Menu", "New Image was set");

    ui->lineEdit->clear();
}

void imageDialog::on_scrBtn_clicked(){
    QScreen *screen = QGuiApplication::primaryScreen();
    if (screen){
        QPixmap inPixmap = screen->grabWindow(QDesktopWidget().winId());
        QByteArray inByteArray;
        QBuffer inBuffer( &inByteArray );
        inBuffer.open( QIODevice::WriteOnly );
        inPixmap.save( &inBuffer, "PNG" );

        //ui->label->setScaledContents(true);
        ui->label->setPixmap(inPixmap);

        Image image;
        image.name = ui->lineEdit->text() + ui->comboBox->currentText();
        image.arr = inByteArray;

        QVariant var = ui->players->currentData();

        emit addImageToDb(image, var.value<Player>().id);
        QMessageBox::information(this, "Image Menu", "New Image was set");

        ui->lineEdit->clear();
    }
}

void imageDialog::on_closeBtn_clicked(){
    this->close();
}

void imageDialog::on_lineEdit_textChanged(const QString &arg1){
    if(arg1.length() == 0){
        ui->scrBtn->setEnabled(false);
        ui->addBtn->setEnabled(false);
    }
    else{
        ui->scrBtn->setEnabled(true);
        ui->addBtn->setEnabled(true);
    }
}

void imageDialog::on_delBtn_clicked(){
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Delete","Are you sure?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes){
        QVariant var = ui->players->currentData();
        emit removeImageFromDb(var.value<Player>().id);
    }
}
