#ifndef CREATE_DIALOG_H
#define CREATE_DIALOG_H

#include <QDialog>
#include <QtSql>
#include <QMessageBox>
#include <QCryptographicHash>

namespace Ui {
class create_dialog;
}

class create_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit create_dialog(QWidget *parent = 0);
    ~create_dialog();

    QString getPassword();
    QString getLogin();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_checkBox_clicked(bool checked);

public slots:
    void user_acception(bool registreted);

private:
    Ui::create_dialog *ui;

signals:
    void registerUser(QString const & username, QString const & password, QString const & fullname);
};

#endif // CREATE_DIALOG_H
