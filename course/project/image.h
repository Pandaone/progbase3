#ifndef IMAGE_H
#define IMAGE_H
#include <QByteArray>
#include <QMetaType>

struct Image{
    QByteArray arr;
    QString name;
};
Q_DECLARE_METATYPE(Image)

#endif // IMAGE_H
