#ifndef SQLSTORAGE_H
#define SQLSTORAGE_H

#include <QtSql>
#include "storage.h"
#include "user.h"


using namespace std;

class SqlStorage : public Storage
{

 protected:
   QSqlDatabase database_;

 public:
   SqlStorage(const string & dir_name);
   SqlStorage();
   ~SqlStorage();

   bool isOpen() const;
   bool open();
   void close();

   vector<Player> getAllPlayers(void);
   optional<Player> getPlayerById(int player_id);
   bool updatePlayer(const Player & player);
   bool removePlayer(int player_id);
   int insertPlayer(Player & player, int user_id = QVariant::Int);

   vector<Team> getAllTeams(void);
   optional<Team> getTeamById(int team_id);
   bool updateTeam(const Team & team);
   bool removeTeam(int team_id);
   int insertTeam(Team & team);

   vector<Player> getAllUserPlayers(int user_id);
   vector<Team> getAllPlayerTeams(int player_id);
   bool insertPlayerTeam(int player_id, int team_id);
   bool removePlayerTeam(int player_id, int team_id);

   QVariant authUser(QString const & username, QString const & password);
   int registerUser(QString const & username, QString const & password, QString const & fullname);

   vector<Player> getModifiedListOfPlayers(int from, const QString &str, const int & user_id, int amount = 10);
   int getTotalAmoutOfEntities(const QString &str, const int & user_id);

   bool insertPlayerImage(const Image & image, int player_id);
   bool removePlayerImage(int player_id);
   QVariant getPlayerImage(int player_id);
};

Player getPlayerFromSql(const QSqlQuery & query);
Team getTeamFromSql(const QSqlQuery & query);
void createTableInDatabase(QString command);
QString hashPassword(QString const & pass);

#endif // SQLSTORAGE_H
