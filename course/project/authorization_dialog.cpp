#include "authorization_dialog.h"
#include "ui_authorizationdialog.h"

AuthorizationDialog::AuthorizationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthorizationDialog)
{
    ui->setupUi(this);
}

AuthorizationDialog::~AuthorizationDialog(){
    delete ui;
}

void AuthorizationDialog::on_buttonBox_accepted(){
    emit userDataEmitter(ui->login->text(), ui->password->text());
}

void AuthorizationDialog::on_buttonBox_rejected(){
    this->reject();
}

void AuthorizationDialog::on_createButton_clicked(){
    create_dialog create(this);

    QObject::connect(&create, &create_dialog::registerUser, this, &AuthorizationDialog::getUserData);
    QObject::connect(this, &AuthorizationDialog::registeredEmitter, &create, &create_dialog::user_acception);

    int status = create.exec();
    if(status == 1) this->accept();
}

void AuthorizationDialog::on_checkBox_clicked(bool checked){
    if(checked) ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
    else ui->password->setEchoMode(QLineEdit::EchoMode::Password);
}

void AuthorizationDialog::userAuthorization(bool exist){
    if(exist) this->accept();
    else{
        int count = ui->tries->text()[0].digitValue() - 1;
        QMessageBox::information(this, "Warning", "Password or login is incorrect");
        ui->tries->setText(QString::number(count) + " tries left");
        if(count == 0) this->reject();
    }
}

void AuthorizationDialog::getUserData(QString const & username, QString const & password, QString const & fullname){
    emit userRegistrationEmitter(username, password, fullname);
}

void AuthorizationDialog::isUserRegistered(bool registered){
    emit registeredEmitter(registered);
}
