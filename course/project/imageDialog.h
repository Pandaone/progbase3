#ifndef IMAGEDIALOG_H
#define IMAGEDIALOG_H

#include <QDialog>
#include <QBuffer>
#include <QScreen>
#include <QDesktopWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QFileDialog>
#include <vector>
#include "image.h"
#include "players.h"
#include <QMessageBox>

namespace Ui {
class imageDialog;
}

class imageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit imageDialog(const vector<Player> & players, QWidget *parent = 0);
    imageDialog();
    ~imageDialog();

private slots:
    void on_addBtn_clicked();

    void on_scrBtn_clicked();

    void on_closeBtn_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_delBtn_clicked();

private:
    Ui::imageDialog *ui;

    const std::vector<Player> players_;

    bool addImage(const Image & image);

signals:
    void addImageToDb(const Image & image, int player_id);

    void removeImageFromDb(int player_id);
};

#endif // IMAGEDIALOG_H
