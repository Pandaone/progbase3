#-------------------------------------------------
#
# Project created by QtCreator 2020-04-16T20:09:52
#
#-------------------------------------------------

QT += core gui
QT += sql

CONFIG += c++1z

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab9
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    editdialog.cpp \
    sqlstorage.cpp \
    storage.cpp \
    authorization_dialog.cpp \
    create_dialog.cpp \
    teamMenu.cpp

HEADERS += \
        mainwindow.h \
    players.h \
    editdialog.h \
    sqlstorage.h \
    storage.h \
    teams.h \
    authorization_dialog.h \
    create_dialog.h \
    user.h \
    teamMenu.h

FORMS += \
        mainwindow.ui \
    editdialog.ui \
    authorizationdialog.ui \
    create_dialog.ui \
    teamMenu.ui
