#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionLogout, &QAction::triggered, this, &MainWindow::Logout);
    connect(ui->actionOpen_storage, &QAction::triggered, this, &MainWindow::OpenStorage);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::Exit);
    connect(ui->actionAddTeam, &QAction::triggered, this, &MainWindow::AddTeam);
    connect(ui->actionJsonImport, &QAction::triggered, this, &MainWindow::importData);
    connect(ui->actionJsonExport, &QAction::triggered, this, &MainWindow::exportData);
    connect(ui->actionNew_Storage, &QAction::triggered, this, &MainWindow::NewStorage);
    connect(ui->actionInfo, &QAction::triggered, this, &MainWindow::userInfo);
    connect(ui->actionImage, &QAction::triggered, this, &MainWindow::ImageMenu);

    storage_ = new SqlStorage;

    ui->name_search->setHidden(true);
    ui->actionLogout->setVisible(false);
    ui->actionAddTeam->setVisible(false);
    ui->actionJsonImport->setVisible(false);
    ui->actionJsonExport->setVisible(false);
    ui->actionInfo->setVisible(false);
    ui->actionImage->setVisible(false);

    ui->AddBtn->setShortcut(Qt::CTRL + Qt::Key_Equal);
    ui->RmBtn->setShortcut(Qt::Key_Delete);

    ui->pageNum->setHidden(true);
    ui->minPage->setHidden(true);
    ui->maxPage->setHidden(true);
    ui->goLeft->setHidden(true);
    ui->goRight->setHidden(true);
}

MainWindow::~MainWindow(){
    delete ui;
    delete storage_;
}

void MainWindow::Logout(){
    if(storage_ == nullptr) return;
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Logout","Are you sure?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes){
        if(storage_ != nullptr) storage_->close();
        ui->listWidget->clear();
        clearLabels();

        ui->RmBtn->setEnabled(false);
        ui->AddBtn->setEnabled(false);
        ui->EditBtn->setEnabled(false);

        ui->actionOpen_storage->setVisible(true);
        ui->actionNew_Storage->setVisible(true);
        ui->actionLogout->setVisible(false);
        ui->actionJsonImport->setVisible(false);
        ui->actionJsonExport->setVisible(false);
        ui->actionInfo->setVisible(false);
        ui->actionImage->setVisible(false);

        ui->name_search->setHidden(true);
        ui->actionAddTeam->setVisible(false);
        ui->name_search->clear();

        ui->pageNum->setHidden(true);
        ui->minPage->setHidden(true);
        ui->maxPage->setHidden(true);
        ui->goLeft->setHidden(true);
        ui->goRight->setHidden(true);

        user.id = -1;
    }
}

void MainWindow::NewStorage(){
    QString fileName = QFileDialog::getSaveFileName(this, "Dialog Caption", "./..", "ALL Files(*)");
    storage_->setName(fileName.toStdString());

    if(storage_->open()){
        AuthorizationDialog auth(this);

        QObject::connect(&auth, &AuthorizationDialog::userDataEmitter, this, &MainWindow::auth_user);
        QObject::connect(this, &MainWindow::user_exist, &auth, &AuthorizationDialog::userAuthorization);

        QObject::connect(&auth, &AuthorizationDialog::userRegistrationEmitter, this, &MainWindow::registration);
        QObject::connect(this, &MainWindow::user_registered, &auth, &AuthorizationDialog::isUserRegistered);

        int status = auth.exec();
        if(status == 1){
            ui->AddBtn->setEnabled(true);
            ui->actionLogout->setVisible(true);
            ui->actionOpen_storage->setVisible(false);
            ui->actionNew_Storage->setVisible(false);
            ui->actionJsonImport->setVisible(true);
            ui->actionJsonExport->setVisible(true);
            ui->actionInfo->setVisible(true);
            ui->actionImage->setVisible(true);

            ui->name_search->setHidden(false);
            if(this->user.login == "Admin") ui->actionAddTeam->setVisible(true);

            ui->pageNum->setHidden(false);
            ui->minPage->setHidden(false);
            ui->maxPage->setHidden(false);
            ui->goLeft->setHidden(false);
            ui->goRight->setHidden(false);

            total_amount = 0;
            ui->maxPage->setText("1");
            ui->goRight->setEnabled(false);
            ui->maxPage->setEnabled(false);
        }
    }
}

void MainWindow::OpenStorage(){
    if(storage_ != nullptr) storage_->close();
    ui->listWidget->clear();
    clearLabels();

    QString fileName = QFileDialog::getOpenFileName(this, "", "./../..", "Sqlite (*.sqlite)");
    storage_->setName(fileName.toStdString());
    if(storage_->open()){
        AuthorizationDialog auth(this);

        QObject::connect(&auth, &AuthorizationDialog::userDataEmitter, this, &MainWindow::auth_user);
        QObject::connect(this, &MainWindow::user_exist, &auth, &AuthorizationDialog::userAuthorization);

        QObject::connect(&auth, &AuthorizationDialog::userRegistrationEmitter, this, &MainWindow::registration);
        QObject::connect(this, &MainWindow::user_registered, &auth, &AuthorizationDialog::isUserRegistered);

        int status = auth.exec();
        if(status == 1){
            ui->AddBtn->setEnabled(true);
            ui->actionLogout->setVisible(true);
            ui->actionOpen_storage->setVisible(false);
            ui->actionNew_Storage->setVisible(false);
            ui->actionJsonImport->setVisible(true);
            ui->actionJsonExport->setVisible(true);
            ui->actionInfo->setVisible(true);
            ui->actionImage->setVisible(true);

            ui->name_search->setHidden(false);
            if(this->user.login == "Admin") ui->actionAddTeam->setVisible(true);

            ui->pageNum->setHidden(false);
            ui->minPage->setHidden(false);
            ui->maxPage->setHidden(false);
            ui->goLeft->setHidden(false);
            ui->goRight->setHidden(false);

            total_amount = storage_->getTotalAmoutOfEntities("", user.id);
            int maxPage = (total_amount % 10 != 0) ? (total_amount / 10 + 1) : (total_amount / 10);
            if(maxPage == 0) maxPage = 1;
            ui->maxPage->setText(QString::number(maxPage));
            if(total_amount - 10 <= 0){
                ui->goRight->setEnabled(false);
                ui->maxPage->setEnabled(false);
            }
            else{
                ui->goRight->setEnabled(true);
                ui->maxPage->setEnabled(true);
            }

            vector<Player> players = storage_->getModifiedListOfPlayers(0, "", user.id);
            fillListWithPlayers(players);
        }
    }
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item){
    QVariant var = item->data(Qt::UserRole);
    Player player = var.value<Player>();
    updateLabels(player);

    ui->photo_name->clear();
    ui->photo_label->clear();

    QVariant varIm = storage_->getPlayerImage(player.id);
    if(varIm.isNull()) return;
    Image image = varIm.value<Image>();
    QPixmap pixmap;
    pixmap.loadFromData(image.arr);
    ui->photo_label->setPixmap(pixmap);
    ui->photo_name->setText(image.name);
}

void MainWindow::on_RmBtn_clicked()
{
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if(list.count() == 0) return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Delete","Are you sure?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes){
        foreach(QListWidgetItem * selectedItem, list){
            QVariant var = selectedItem->data(Qt::UserRole);
            Player player = var.value<Player>();
            total_amount--;
            storage_->removePlayer(player.id);
        }
        clearLabels();
        ui->listWidget->clear();
    }
    int maxPage = (total_amount % 10 != 0) ? (total_amount / 10 + 1) : (total_amount / 10);
    if(maxPage == 0) maxPage = 1;
    ui->maxPage->setText(QString::number(maxPage));
    int currentPage = ui->pageNum->text().toInt();
    if(currentPage > maxPage){
        currentPage--;
        ui->pageNum->setText(QString::number(currentPage));
    }
    if(currentPage == maxPage){
        ui->maxPage->setEnabled(false);
        ui->goRight->setEnabled(false);
    }
    if(currentPage == 1){
        ui->minPage->setEnabled(false);
        ui->goLeft->setEnabled(false);
    }
    vector<Player> players = storage_->getModifiedListOfPlayers(0, ui->name_search->text(), user.id);
    fillListWithPlayers(players);
}

void MainWindow::clearLabels(){
    ui->label_id->clear();
    ui->label_age->clear();
    ui->label_country->clear();
    ui->label_name->clear();
    ui->label_pos->clear();
    ui->label_team->clear();
    ui->photo_name->clear();
    ui->photo_label->clear();
}

void MainWindow::on_AddBtn_clicked(){
    if(storage_ == nullptr || !storage_->isOpen()) return;

    EditDialog addDialog(this);
    addDialog.setPlaceHolderText();

    int status = addDialog.exec();
    if(status == 1){
        Player player = addDialog.getData();
        player.id = storage_->insertPlayer(player, user.id);
        addPlayer(player);
    }
}

void MainWindow::on_EditBtn_clicked(){
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if(list.count() == 0) return;

    else if(list.count() == 1){
        QListWidgetItem * item = list.at(0);
        QVariant var = item->data(Qt::UserRole);
        Player player = var.value<Player>();

        vector<Team> PlayerTeams = storage_->getAllPlayerTeams(player.id);
        vector<Team> PossiblePlayerTeams = storage_->getAllTeams();
        for(auto i = PossiblePlayerTeams.begin(); i != PossiblePlayerTeams.end(); i++){
            for(auto j = PlayerTeams.begin(); j != PlayerTeams.end(); j++){
                if(i->id == j->id){
                    PossiblePlayerTeams.erase(i);
                    i--;
                    break;
                }
            }
        }
        EditDialog editDialog(this);
        editDialog.setData(player);
        editDialog.setTeamsData(PlayerTeams);
        editDialog.setAllTeamsData(PossiblePlayerTeams);

        connect(&editDialog, &EditDialog::PlayerTeam, this, &MainWindow::PlayerTeam);
        int status = editDialog.exec();

        if(status == 1){
            Player playerE = editDialog.getData();
            playerE.id = player.id;
            item->setText(QString::fromStdString(playerE.name));
            QVariant var = QVariant::fromValue(playerE);
            item->setData(Qt::UserRole, var);
            storage_->updatePlayer(playerE);
            updateLabels(playerE);
        }
    } else QMessageBox::information(this, "Warning", "To change data of palyer you have to choose only one!!!");
}

void MainWindow::updateLabels(const Player & player){
    ui->label_id->setText("Id: " + QString::number(player.id));
    ui->label_name->setText("Name: " + QString::fromStdString(player.name));
    ui->label_age->setText("Age: " + QString::number(player.age));
    ui->label_pos->setText("Position: " + QString::fromStdString(player.position));
    ui->label_team->setText("Team: " + QString::fromStdString(player.team));
    ui->label_country->setText("Country: " + QString::fromStdString(player.country));
}

void MainWindow::Exit(){
    int reply = QMessageBox::question(this, "Exit", "Are you sure");
    if(reply == QMessageBox::Yes) QMainWindow::close();
}

void MainWindow::PlayerTeam(int player_id, int team_id, bool code){
    if(code) storage_->insertPlayerTeam(player_id, team_id);
    else storage_->removePlayerTeam(player_id, team_id);
}

void MainWindow::auth_user(const QString & login, const QString & password){
    QVariant user = storage_->authUser(login, password);
    if(user.isNull()) emit user_exist(false);
    else{
        emit user_exist(true);
        this->user = user.value<User>();
    }
}

void MainWindow::registration(QString const & username, QString const & password, QString const & fullname){
    int id = storage_->registerUser(username, password, fullname);
    if(id == -100) emit user_registered(false);
    else{
        emit user_registered(true);
        user.id = id;
    }
}

void MainWindow::on_name_search_textChanged(const QString &arg1){
    ui->listWidget->clear();
    clearLabels();

    total_amount = storage_->getTotalAmoutOfEntities(arg1, user.id);
    int maxPage = (total_amount % 10 != 0)? (total_amount / 10 + 1) : total_amount / 10;
    ui->maxPage->setText(QString::number(maxPage));
    if(total_amount - 10 <= 0){
        ui->goRight->setEnabled(false);
        ui->maxPage->setEnabled(false);
    }
    else{
        ui->goRight->setEnabled(true);
        ui->maxPage->setEnabled(true);
    }
    ui->goLeft->setEnabled(false);
    ui->minPage->setEnabled(false);
    ui->pageNum->setText("1");

    vector<Player> players = storage_->getModifiedListOfPlayers(0, arg1, user.id);
    fillListWithPlayers(players);
}

void MainWindow::AddTeam(){
    teamMenu createTeam(this);
    QObject::connect(&createTeam, &teamMenu::createTeam, this, &MainWindow::createTeam);

    int status = createTeam.exec();
    if(status == 1) QMessageBox::information(this, "Congratulation", "You have created your own team");
}

void MainWindow::createTeam(const QString & name, const QString & country, const int & year, const QString & stadium){
    Team team;
    team.name = name.toStdString();
    team.country = country.toStdString();
    team.YearFound = year;
    team.stadium = stadium.toStdString();
    storage_->insertTeam(team);
}

void MainWindow::on_goLeft_clicked(){
    ui->listWidget->clear();
    clearLabels();

    int page_num = ui->pageNum->text().toInt() - 1;
    ui->pageNum->setText(QString::number(page_num));

    vector<Player> players = storage_->getModifiedListOfPlayers((page_num - 1) * 10, ui->name_search->text(), user.id);
    fillListWithPlayers(players);

    if(page_num == 1){
        ui->goLeft->setEnabled(false);
        ui->minPage->setEnabled(false);
    }
    ui->goRight->setEnabled(true);
    ui->maxPage->setEnabled(true);
}

void MainWindow::on_minPage_clicked(){
    ui->listWidget->clear();
    clearLabels();

    ui->pageNum->setText("1");

    vector<Player> players = storage_->getModifiedListOfPlayers(0, ui->name_search->text(), user.id);
    fillListWithPlayers(players);

    ui->goLeft->setEnabled(false);
    ui->minPage->setEnabled(false);
    ui->goRight->setEnabled(true);
    ui->maxPage->setEnabled(true);
}

void MainWindow::on_maxPage_clicked(){
    ui->listWidget->clear();
    clearLabels();

    int page_num = ui->maxPage->text().toInt();
    ui->pageNum->setText(QString::number(page_num));

    vector<Player> players = storage_->getModifiedListOfPlayers((page_num - 1) * 10, ui->name_search->text(), user.id);
    fillListWithPlayers(players);

    ui->goLeft->setEnabled(true);
    ui->minPage->setEnabled(true);
    ui->goRight->setEnabled(false);
    ui->maxPage->setEnabled(false);
}

void MainWindow::on_goRight_clicked(){
    ui->listWidget->clear();
    clearLabels();

    int page_num = ui->pageNum->text().toInt();
    ui->pageNum->setText(QString::number(page_num + 1));

    vector<Player> players = storage_->getModifiedListOfPlayers(page_num * 10, ui->name_search->text(), user.id);
    fillListWithPlayers(players);

    if(total_amount - (page_num + 1) * 10 <= 0){
        ui->goRight->setEnabled(false);
        ui->maxPage->setEnabled(false);
    }
    ui->goLeft->setEnabled(true);
    ui->minPage->setEnabled(true);
}

void MainWindow::fillListWithPlayers(const vector<Player> & players){
    foreach(Player player, players){
        QListWidgetItem * newItem = new QListWidgetItem(QString::fromStdString(player.name));
        QVariant var = QVariant::fromValue(player);
        newItem->setData(Qt::UserRole, var);
        ui->listWidget->addItem(newItem);
    }
}

void MainWindow::on_listWidget_itemSelectionChanged(){
    int count = ui->listWidget->selectedItems().count();
    if(count == 0){
        ui->RmBtn->setEnabled(false);
        ui->EditBtn->setEnabled(false);
    } else if(count == 1){
        ui->RmBtn->setEnabled(true);
        ui->EditBtn->setEnabled(true);
    } else{
        ui->RmBtn->setEnabled(true);
        ui->EditBtn->setEnabled(false);
    }
}

void MainWindow::importData(){
    QString fileName = QFileDialog::getOpenFileName(this, "", "./../..", "Json (*.json)");
    QFile inFile(fileName);
    if(!inFile.open(QIODevice::ReadOnly)) return;
    QString qStr = QString::fromStdString(inFile.readAll().toStdString());

    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(qStr.toUtf8(), &err);
    if (err.error != QJsonParseError::NoError) {
        QMessageBox::information(this, "Error", "Json file is incorrect");
        return;
    }

    QJsonArray playerArr = doc.array();
    for(QJsonValue playerVal : playerArr){
        QJsonObject playerObj = playerVal.toObject();
        vector<Team> teams;
        Player player = jsonObjToPlayer(playerObj, teams);
        int player_id = storage_->insertPlayer(player, user.id);
        for(Team team : teams){
            int team_id =  storage_->insertTeam(team);
            storage_->insertPlayerTeam(player_id, team_id);
        }
        addPlayer(player);
    }

}

void MainWindow::exportData(){
    QJsonDocument doc;
    QJsonArray playersArray;

   vector<Player> players = storage_->getAllUserPlayers(user.id);
   foreach(Player player, players){
        vector<Team> teams = storage_->getAllPlayerTeams(player.id);
        QJsonObject playerObj = playerToJsonObj(player, teams);
        playersArray.push_back(playerObj);
   }
   doc.setArray(playersArray);
   QString qStr = doc.toJson(QJsonDocument::JsonFormat::Indented);
   QString fileName = QFileDialog::getSaveFileName(this, "Dialog Caption", "./../..", "ALL Files(*)");
   QFile outFile(fileName);
   outFile.open(QIODevice::WriteOnly);
   QTextStream outStream(&outFile);
   outStream << qStr;
}

void MainWindow::addPlayer(const Player & player){
    total_amount++;
    if(ui->listWidget->count() < 10){
        QListWidgetItem * newItem = new QListWidgetItem(QString::fromStdString(player.name));
        QVariant var = QVariant::fromValue(player);
        newItem->setData(Qt::UserRole, var);
        ui->listWidget->addItem(newItem);
        return;
    }
    int maxPageNew = (total_amount % 10 != 0) ? (total_amount / 10 + 1) : (total_amount / 10);
    int maxPageCurrent = ui->maxPage->text().toInt();
    if(maxPageNew > maxPageCurrent){
        ui->maxPage->setText(QString::number(maxPageNew));
        ui->maxPage->setEnabled(true);
        ui->goRight->setEnabled(true);

    }
}
void MainWindow::userInfo(){
    QMessageBox::information(this, QString::number(user.id), "name:  " +
                             user.name + "\n" + "login:  " + user.login);
}

void MainWindow::ImageMenu(){
    if(ui->listWidget->count() == 0){
        QMessageBox::information(this, "Warning", "You should have players to add images");
        return;
    }

    imageDialog imageMenu(storage_->getAllUserPlayers(user.id), this);

    connect(&imageMenu, &imageDialog::addImageToDb, this, &MainWindow::addImageToDb);
    connect(&imageMenu, &imageDialog::removeImageFromDb, this, &MainWindow::removeImageFromDb);

    imageMenu.exec();
}

void MainWindow::addImageToDb(const Image & image, int player_id){
    storage_->insertPlayerImage(image, player_id);
}

void MainWindow::removeImageFromDb(int player_id){
    storage_->removePlayerImage(player_id);
}
