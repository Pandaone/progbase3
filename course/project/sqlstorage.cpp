#include "sqlstorage.h"

SqlStorage::SqlStorage(const string & dir_name) : Storage(dir_name) {
    database_ = QSqlDatabase::addDatabase("QSQLITE");
    open();
}

SqlStorage::SqlStorage(){
    database_ = QSqlDatabase::addDatabase("QSQLITE");
}

SqlStorage::~SqlStorage(){
    close();
    QSqlDatabase::removeDatabase("QSQLITE");
}

bool SqlStorage::isOpen() const{
    if(database_.isOpen()) return true;
    return false;
}

bool SqlStorage::open(){
    if(database_.isOpen()) return true;
    QString path = QString::fromStdString(name());
    if(path.indexOf("sqlite") < 0) return false;
    database_.setDatabaseName(path);

    bool connected = database_.open();
    if (!connected) return false;

    QString table_teams = "CREATE TABLE IF NOT EXISTS teams "
                            "(id INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,"
                            "name TEXT NOT NULL UNIQUE,"
                            "country TEXT NOT NULL,"
                            "year INTEGER NOT NULL,"
                            "stadium TEXT NOT NULL);";
    createTableInDatabase(table_teams);

    QString table_users = "CREATE TABLE IF NOT EXISTS users "
                          "(id INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,"
                          "username TEXT NOT NULL UNIQUE,"
                          "password_hash TEXT NOT NULL,"
                          "fullname TEXT NOT NULL);";
    createTableInDatabase(table_users);

    QString table_players = "CREATE TABLE IF NOT EXISTS players "
                          "(id INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,"
                          "name TEXT NOT NULL,"
                          "age INTEGER NOT NULL,"
                          "position TEXT NOT NULL,"
                          "team TEXT NOT NULL,"
                          "country TEXT NOT NULL,"
                          "user_id INTEGER,"
                          "FOREIGN KEY(\"user_id\") REFERENCES \"users\"(\"id\"));";
    createTableInDatabase(table_players);

    QString table_links = "CREATE TABLE IF NOT EXISTS links "
                          "(id INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,"
                          "player_id INTEGER NOT NULL,"
                          "team_id INTEGER NOT NULL,"
                          "FOREIGN KEY(\"player_id\") REFERENCES \"players\"(\"id\"),"
                          "FOREIGN KEY(\"team_id\") REFERENCES \"teams\"(\"id\"));";
    createTableInDatabase(table_links);

    QString table_images = "CREATE TABLE IF NOT EXISTS images "
                           "(id INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,"
                           "image BLOB NOT NULL,"
                           "player_id INTEGER NOT NULL UNIQUE,"
                           "name TEXT NOT NULL,"
                           "FOREIGN KEY(\"player_id\") REFERENCES \"players\"(\"id\"));";
    createTableInDatabase(table_images);

    return true;
}

void SqlStorage::close(){
    database_.close();
}

vector<Player> SqlStorage::getAllPlayers(void){
    open();
    vector<Player> players;
    QSqlQuery query("SELECT * FROM players");
    while (query.next()){
       Player player = getPlayerFromSql(query);
       players.push_back(player);
    }
    return players;
}

optional<Player> SqlStorage::getPlayerById(int player_id){
    open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM players WHERE id = :id")){
       qDebug() << "get person query prepare error:" << query.lastError();
       exit(1);
    }
    query.bindValue(":id", player_id);
    if (!query.exec()){
       qDebug() << "get person query exec error:" << query.lastError();
       exit(1);
    }
    if (query.next()){
       optional<Player> player = getPlayerFromSql(query);
       return player;
    } else return nullopt;
}

bool SqlStorage::updatePlayer(const Player & player){
    open();
    QSqlQuery query;
    if (!query.prepare("UPDATE players SET name = :name, age = :age, position = :pos, team = :team, country = :country WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:" << query.lastError();
        exit(1);
    }
    query.bindValue(":name", QString::fromStdString(player.name));
    query.bindValue(":age", player.age);
    query.bindValue(":pos", QString::fromStdString(player.position));
    query.bindValue(":team", QString::fromStdString(player.team));
    query.bindValue(":country", QString::fromStdString(player.country));
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:" << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;
    return true;
}

bool SqlStorage::removePlayer(int player_id){
    open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM players WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:" << query.lastError();
        exit(1);
    }
    query.bindValue(":id", player_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:" << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;

    if (!query.prepare("DELETE FROM links WHERE player_id = :id")){
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":id", player_id);
    if (!query.exec()){
        qDebug() << query.lastError();
        exit(1);
    }
    return true;
}

int SqlStorage::insertPlayer(Player & player, int user_id){
    open();
    QSqlQuery query;
    if (!query.prepare("INSERT INTO players (name, age, position, team, country, user_id) VALUES (:name, :age, :pos, :team, :country, :user_id)")){
       qDebug() << "addPerson query prepare error:" << query.lastError();
       exit(1);
    }
    query.bindValue(":name", QString::fromStdString(player.name));
    query.bindValue(":age", player.age);
    query.bindValue(":pos", QString::fromStdString(player.position));
    query.bindValue(":team", QString::fromStdString(player.team));
    query.bindValue(":country", QString::fromStdString(player.country));
    query.bindValue(":user_id", user_id);
    if (!query.exec()){
       qDebug() << "addPerson query exec error:" << query.lastError();
       exit(1);
    }
    return query.lastInsertId().toInt();
}

vector<Team> SqlStorage::getAllTeams(void){
    open();
    vector<Team> teams;
    QSqlQuery query("SELECT * FROM teams");
    while (query.next()){
       Team team = getTeamFromSql(query);
       teams.push_back(team);
    }
    return teams;
}

optional<Team> SqlStorage::getTeamById(int team_id){
    open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM teams WHERE id = :id")){
       qDebug() << "get person query prepare error:" << query.lastError();
       exit(1);
    }
    query.bindValue(":id", team_id);
    if (!query.exec()){
       qDebug() << "get person query exec error:" << query.lastError();
       exit(1);
    }
    if (query.next()){
       optional<Team> team = getTeamFromSql(query);
       return team;
    } else return nullopt;
}

bool SqlStorage::updateTeam(const Team & team){
    open();
    QSqlQuery query;
    if (!query.prepare("UPDATE teams SET name = :name, country = :country, year = :year, stadium = :stadium WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:" << query.lastError();
        exit(1);
    }
    query.bindValue(":name", QString::fromStdString(team.name));
    query.bindValue(":id", team.id);
    query.bindValue(":year", team.YearFound);
    query.bindValue(":stadium", QString::fromStdString(team.stadium));
    query.bindValue(":country", QString::fromStdString(team.country));
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:" << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;
    return true;
}

bool SqlStorage::removeTeam(int team_id){
    open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM teams WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:" << query.lastError();
        exit(1);
    }
    query.bindValue(":id", team_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:" << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;

    if (!query.prepare("DELETE FROM links WHERE team_id = :id")){
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":id", team_id);
    if (!query.exec()){
        qDebug() << query.lastError();
        exit(1);
    }
    return true;
}

int SqlStorage::insertTeam(Team & team){
    open();
    QSqlQuery query;
    if (!query.prepare("INSERT INTO teams (name, country, year, stadium) VALUES (:name, :country, :year, :stadium)")){
       qDebug() << "addPerson query prepare error:" << query.lastError();
       exit(1);
    }
    query.bindValue(":name", QString::fromStdString(team.name));
    query.bindValue(":year", team.YearFound);
    query.bindValue(":stadium", QString::fromStdString(team.stadium));
    query.bindValue(":country", QString::fromStdString(team.country));
    if (!query.exec()){
        QSqlQuery query2;
        if (!query2.prepare("SELECT * FROM teams WHERE name = :name")){
           qDebug() << query2.lastError();
           exit(1);
        }
        query2.bindValue(":name", QString::fromStdString(team.name));
        if (!query2.exec()){
           qDebug() << query2.lastError();
           exit(1);
        }
        if(!query2.next()) exit(1);
        return query2.value("id").toInt();
    }
    return query.lastInsertId().toInt();
}

Player getPlayerFromSql(const QSqlQuery & query){
    Player player;
    player.id = query.value("id").toInt();
    player.name = query.value("name").toString().toStdString();
    player.age = query.value("age").toInt();
    player.position = query.value("position").toString().toStdString();
    player.team = query.value("team").toString().toStdString();
    player.country = query.value("country").toString().toStdString();
    return player;
}

Team getTeamFromSql(const QSqlQuery & query){
    Team team;
    team.id = query.value("id").toInt();
    team.name = query.value("name").toString().toStdString();
    team.country = query.value("country").toString().toStdString();
    team.YearFound = query.value("year").toInt();
    team.stadium = query.value("stadium").toString().toStdString();
    return team;
}

void createTableInDatabase(QString command){
    QSqlQuery query;
    if(!query.prepare(command)){
        qDebug() << query.lastError();
        exit(1);
    }
    if(!query.exec()){
        qDebug() << query.lastError();
        exit(1);
    }
}

vector<Player> SqlStorage::getAllUserPlayers(int user_id){
    open();
    vector<Player> players;
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM players WHERE user_id = :id")){
       qDebug() << query.lastError();
       exit(1);
    }
    query.bindValue(":id", user_id);
    if (!query.exec()){
       qDebug() << query.lastError();
       exit(1);
    }
    while(query.next()){
       Player player = getPlayerFromSql(query);
       players.push_back(player);
    }
    return players;
}

vector<Team> SqlStorage::getAllPlayerTeams(int player_id){
    vector<Team> teams;
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM links WHERE player_id = :id")){
       qDebug() << query.lastError();
       exit(1);
    }
    query.bindValue(":id", player_id);
    if (!query.exec()){
       qDebug() << query.lastError();
       exit(1);
    }
    while(query.next()){
       int id = query.value("team_id").toInt();
       optional<Team> team = getTeamById(id);
       if(!team) exit(1);
       teams.push_back(team.value());
    }
    return teams;
}

bool SqlStorage::insertPlayerTeam(int player_id, int team_id){
    open();
    QSqlQuery query;
    if (!query.prepare("INSERT INTO links (player_id, team_id) VALUES (:p_id, :t_id)")){
       qDebug() << "addPerson query prepare error:" << query.lastError();
       exit(1);
    }
    query.bindValue(":p_id", player_id);
    query.bindValue(":t_id", team_id);
    if (!query.exec()){
       qDebug() << query.lastError();
       return false;
    }
    return true;
}

bool SqlStorage::removePlayerTeam(int player_id, int team_id){
    open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM links WHERE player_id = :p_id AND team_id = :t_id")){
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":p_id", player_id);
    query.bindValue(":t_id", team_id);
    if (!query.exec()){
        qDebug() << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;
    return true;
}

QVariant SqlStorage::authUser(QString const & username, QString const & password){
   open();
   QSqlQuery query;
   QString sql = "SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;";
   if (!query.prepare(sql)) {
       qDebug() << query.lastError();
       exit(1);
   }
   query.bindValue(":username", username);
   query.bindValue(":password_hash", hashPassword(password));
   if (!query.exec()) {
       qDebug() << query.lastError();
       exit(1);
   }
   if (query.next()){
       User user;
       user.id = query.value("id").toInt();
       user.login = username;
       user.name = query.value("fullname").toString();
       return QVariant::fromValue(user);
   }
   return QVariant::fromValue(nullptr);
}

int SqlStorage::registerUser(QString const & username, QString const & password, QString const & fullname){
    open();

    QSqlQuery query1;
    if (!query1.prepare("SELECT * FROM users WHERE username = :login")){
       qDebug() << query1.lastError();
       exit(1);
    }
    query1.bindValue(":login", username);
    if (!query1.exec()){
       qDebug() << query1.lastError();
       exit(1);
    }
    if (query1.next()) return -100;

    QSqlQuery query;
    QString sql = "INSERT INTO users (username, password_hash, fullname) VALUES (:username, :password_hash, :fullname);";
    if (!query.prepare(sql)) {
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":username", username);
    query.bindValue(":password_hash", hashPassword(password));
    query.bindValue(":fullname", fullname);
    if (!query.exec()) {
        qDebug() << query.lastError();
        exit(1);
    }
    QVariant id = query.lastInsertId();
    return id.toInt();
}

QString hashPassword(QString const & pass){
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

vector<Player> SqlStorage::getModifiedListOfPlayers(int from, const QString &str, const int & user_id, int amount){
    open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM players WHERE user_id = :u_id AND name LIKE :name LIMIT :amount OFFSET :from")){
       qDebug() << query.lastError();
       exit(1);
    }
    query.bindValue(":u_id", user_id);
    query.bindValue(":name", "%" + str + "%");
    query.bindValue(":amount", amount);
    query.bindValue(":from", from);
    if (!query.exec()) {
        qDebug() << query.lastError();
        exit(1);
    }
    vector<Player> players = {};
    while (query.next()){
       Player player = getPlayerFromSql(query);
       players.push_back(player);
    }
    return players;
}

int SqlStorage::getTotalAmoutOfEntities(const QString &str, const int & user_id){
    open();
    QSqlQuery query;
    if (!query.prepare("SELECT COUNT(*) FROM players WHERE user_id = :u_id AND name LIKE :name")){
       qDebug() << query.lastError();
       exit(1);
    }
    query.bindValue(":u_id", user_id);
    query.bindValue(":name", "%" + str + "%");
    if (!query.exec()) {
        qDebug() << query.lastError();
        exit(1);
    }
    query.next();
    int number = query.value(0).toInt();
    return number;
}

bool SqlStorage::insertPlayerImage(const Image & image, int player_id){
    QSqlQuery query;
    QString sql = "INSERT INTO images (image, name, player_id) VALUES (:image, :name, :player_id);";
    if (!query.prepare(sql)) {
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":image", image.arr);
    query.bindValue(":name", image.name);
    query.bindValue(":player_id", player_id);
    if (!query.exec()) {
        QSqlQuery query1;
        if (!query1.prepare("UPDATE images SET image = :image, name = :name WHERE player_id = :player_id")){
            qDebug() << query1.lastError();
            exit(1);
        }
        query1.bindValue(":name", image.name);
        query1.bindValue(":player_id", player_id);
        query1.bindValue(":image", image.arr);
        if (!query1.exec()){
            qDebug() << query1.lastError();
            exit(1);
        }
        if(query1.numRowsAffected() == 0) return false;
        return true;
    }
    return true;
}

bool SqlStorage::removePlayerImage(int player_id){
    open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM images WHERE player_id = :p_id")){
        qDebug() << query.lastError();
        exit(1);
    }
    query.bindValue(":p_id", player_id);
    if (!query.exec()){
        qDebug() << query.lastError();
        exit(1);
    }
    if(query.numRowsAffected() == 0) return false;
    return true;
}

QVariant SqlStorage::getPlayerImage(int player_id){
    open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM images WHERE player_id = :p_id")){
       qDebug() << query.lastError();
       exit(1);
    }
    query.bindValue(":p_id", player_id);
    if (!query.exec()) {
        qDebug() << query.lastError();
        exit(1);
    }
    if(query.next()){
        Image image;
        image.arr = query.value("image").toByteArray();
        image.name = query.value("name").toString();
        return QVariant::fromValue(image);
    }
    return QVariant::fromValue(nullptr);
}
