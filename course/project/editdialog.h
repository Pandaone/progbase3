#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include "players.h"
#include "teams.h"
#include <vector>
#include <QListWidgetItem>
#include <QMessageBox>

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    ~EditDialog();

    void setData(const Player & player);
    Player getData();
    void setPlaceHolderText();

    void setTeamsData(const vector<Team> & teams);
    void setAllTeamsData(const vector<Team> & teams);

private slots:
    void on_RmTeam_clicked();
    void on_AddTeam_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_infoBtn_clicked();

    void on_listWidget_itemSelectionChanged();

private:
    Ui::EditDialog *ui;

    int id;

signals:
    void PlayerTeam(int player_id, int team_id, bool code);
};

#endif // EDITDIALOG_H
