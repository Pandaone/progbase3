#ifndef AUTHORIZATION_DIALOG_H
#define AUTHORIZATION_DIALOG_H

#include "create_dialog.h"

namespace Ui {
class AuthorizationDialog;
}

class AuthorizationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthorizationDialog(QWidget *parent = 0);
    ~AuthorizationDialog();

private slots:

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_createButton_clicked();

    void on_checkBox_clicked(bool checked);

    void getUserData(QString const & username, QString const & password, QString const & fullname);

public slots:
    void userAuthorization(bool exist);

    void isUserRegistered(bool registered);

private:
    Ui::AuthorizationDialog *ui;

signals:
    void userDataEmitter(const QString & login, const QString & password);

    void userRegistrationEmitter(QString const & username, QString const & password, QString const & fullname);
    void registeredEmitter(bool registered);
};

#endif // AUTHORIZATION_DIALOG_H
