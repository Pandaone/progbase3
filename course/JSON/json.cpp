#include "json.h"

Player jsonObjToPlayer(const QJsonObject & jobj, vector<Team> & teams){
    Player p;
    p.name = jobj.value("name").toString().toStdString();
    p.age = jobj.value("age").toInt();
    p.position = jobj.value("pos").toString().toStdString();
    p.team = jobj.value("team").toString().toStdString();
    p.country = jobj.value("country").toString().toStdString();
    QJsonArray teamsArr = jobj.value("teams").toArray();
    for(QJsonValue teamVal : teamsArr){
        QJsonObject teamObj = teamVal.toObject();
        teams.push_back(jsonObjToTeam(teamObj));
    }
    return p;
}

QJsonObject playerToJsonObj(const Player & p, const vector<Team> & teams){
    QJsonObject jobj;
    jobj.insert("name", QString::fromStdString(p.name));
    jobj.insert("age", p.age);
    jobj.insert("pos", QString::fromStdString(p.position));
    jobj.insert("team", QString::fromStdString(p.team));
    jobj.insert("country", QString::fromStdString(p.country));
    QJsonArray teamsArray;
    foreach(Team team, teams){
        QJsonObject teamObj = teamToJsonObj(team);
        teamsArray.push_back(teamObj);
    }
    jobj.insert("teams", teamsArray);
    return jobj;
}

Team jsonObjToTeam(const QJsonObject & jobj){
    Team t;
    t.name = jobj.value("name").toString().toStdString();
    t.country = jobj.value("country").toString().toStdString();
    t.YearFound = jobj.value("year").toInt();
    t.stadium = jobj.value("stadium").toString().toStdString();
    return t;
}

QJsonObject teamToJsonObj(const Team & t){
    QJsonObject jobj;
    jobj.insert("name", QString::fromStdString(t.name));
    jobj.insert("year", t.YearFound);
    jobj.insert("stadium", QString::fromStdString(t.stadium));
    jobj.insert("country", QString::fromStdString(t.country));
    return jobj;
}
