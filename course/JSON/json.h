#ifndef JSON_H
#define JSON_H

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <players.h>
#include<teams.h>

Player jsonObjToPlayer(const QJsonObject & jobj, vector<Team> & teams);
QJsonObject playerToJsonObj(const Player & player, const vector<Team> & teams);

Team jsonObjToTeam(const QJsonObject & jobj);
QJsonObject teamToJsonObj(const Team & team);

#endif // JSON_H
