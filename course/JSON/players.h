#pragma once
#include <iostream>
#include <QMetaType>

using namespace std;

struct Player{
    int id = 0;
    string name;
    int age;
    string position;
    string team;
    string country;
};

Q_DECLARE_METATYPE(Player)
