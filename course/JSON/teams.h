#pragma once
#include <iostream>
#include <QMetaType>

using namespace std;

struct Team{
    int id;
    string name;
    string country;
    int YearFound;
    string stadium;
};

Q_DECLARE_METATYPE(Team)
